'use_strict';

const amqp = require('amqplib/callback_api');
const queue = 'default';

amqp.connect('amqp://localhost', function(err, conn) {
    conn.createChannel(function(err, channel) {
        channel.assertQueue(queue, {durable: true}, function(err, ok) {
            console.log(ok);
        });
        channel.consume(queue, function(msg) {
            let secs = msg.content.toString().split('.').length - 1;
            /**
             * ТО что было  в очереди (сообщение)
             */
            console.log(" [x] Received %s", msg.content.toString());
            setTimeout(function() {
                /**
                 * Считано сообщение
                 */
                console.log(" [x] Done");
            }, secs * 1000);
        }, {
            noAck: true
        });
    });
});